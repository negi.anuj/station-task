import {View, Text, Image, TouchableWithoutFeedback} from 'react-native';
import React, {useState, useEffect} from 'react';
import styles from '../../../styles';
import ButtonSlim from '../../components/common/ButtonSlim';
import AsyncStorage from '@react-native-async-storage/async-storage';

var interval = null;
var time = null;
var pauseGlobal = false;

const StationDetails = props => {
  const [detail] = useState(props.route.params.state);
  const [seconds, setSeconds] = useState(10);
  const [pause, setPause] = useState(false);

  useEffect(() => {
    time = 0;
    pauseGlobal = false;
    AsyncStorage.getItem(`${props.route.params.state.id}`).then(sec => {
      if (sec) {
        const seconds = JSON.parse(sec);
        time = seconds;
        setSeconds(time);
      } else {
        time = 10;
        setSeconds(10);
      }
    });
  }, []);

  useEffect(() => {
    interval = setInterval(() => {
      if (!pauseGlobal) {
        if (time && time >= 0) {
          const sec = time - 1;
          time = time - 1;
          setSeconds(sec);
          AsyncStorage.setItem(`${detail.id}`, JSON.stringify(sec));
        }
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [pauseGlobal, seconds]);

  const handlePauseToggle = () => {
    pauseGlobal = !pause;
    setPause(!pause);
  };

  return (
    <View style={[styles.wrapper]}>
      <Image
        style={{position: 'absolute', zIndex: -1, top: -140}}
        source={require('../../../assets/image/bottomBg.png')}
      />
      <View style={[styles.flexCenterColumn, {marginTop: 40}]}>
        <View
          style={{
            flexDirection: 'row',
            width: '63%',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 30,
            paddingHorizontal: 20,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              props.navigation.pop();
            }}>
            <View style={{padding:5}}>
              <Image source={require('../../../assets/image/back.png')} />
            </View>
          </TouchableWithoutFeedback>
          <Text style={[styles.headerTextBold, {marginTop: 0}]}>Details</Text>
        </View>
        <View style={[styles.contentWrapper, styles.flexCenterColumn]}>
          <Text style={[styles.headerTextBold, {textAlign: 'left'}]}>
            {!pause ? 'Station Subscribed' : 'Subscribe Station'}
          </Text>

          <View style={[styles.detailCard]}>
            <Text style={[styles.activeHeading]}>ACTIVE FROM</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                marginTop: 10,
              }}>
              <View style={[styles.flexRow]}>
                <Text style={styles.secondTimer}>{seconds}</Text>
                <Text style={[styles.secondsText]}>seconds</Text>
              </View>
              <TouchableWithoutFeedback onPress={() => handlePauseToggle()}>
                <View>
                  <ButtonSlim
                    text={
                      <View style={[styles.flexCenter]}>
                        <Text style={[styles.btnText, {fontSize: 12}]}>
                          {!pause ? 'Stop' : 'Resume'}
                        </Text>
                      </View>
                    }
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={[styles.flexRow, styles.alignCenter, {marginTop: 10}]}>
              <Text style={[styles.secondsText]}>MORE INFO</Text>
              <Image
                source={require('../../../assets/image/down.png')}
                style={{marginLeft: 10}}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default StationDetails;
