import {View, Text, Image, TouchableWithoutFeedback} from 'react-native';
import React from 'react';
import styles from '../../../styles';
import Button from '../../components/common/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Disclaimer = props => {
  const acceptSubmit = () => {
    AsyncStorage.setItem('disclaimer', JSON.stringify('true'))
      .then(res => {
        props.navigation.reset({
          index: 0,
          routes: [{name: 'StationHome'}],
        });
      })
      .catch(err => {
        console.log('disError', err);
      });
  };

  return (
    <View style={[styles.wrapper]}>
      <Image
        style={{position: 'absolute', zIndex: -1, top: -140}}
        source={require('../../../assets/image/bottomBg.png')}
      />
      <View
        style={[
          styles.flexCenterColumn,
          {marginTop: 40, alignItems: 'center'},
        ]}>
        <Image source={require('../../../assets/image/icon.png')} />
        <View
          style={[
            styles.contentWrapper,
            styles.alignCenter,
            styles.flexCenterColumn,
          ]}>
          <Text style={[styles.headerTextBold]}>Disclaimer</Text>
          <Text style={[styles.paraLight, {marginTop: 20}]}>
            The information provided by the Zdaly Fuel Network Optimizer app is
            based on historical data. Data on Zdaly Light is updated once daily
            at 8:00 a.m. eastern time. Any prospective information is based on
            that data and should not be relied on as a estimation of future
            performance. Any future product prices are the manufacturer's
            suggested retail price (MSRP) only. Sites are independent operators
            free to set their retail price.
          </Text>
          <TouchableWithoutFeedback
            onPress={() => {
              acceptSubmit();
            }}>
            <View style={{margin: 40}}>
              <Button
                text={
                  <View
                    style={[
                      styles.flexCenter,
                      {
                        width: 150,
                      },
                    ]}>
                    <Text style={[styles.btnText]}>I Accept</Text>
                  </View>
                }
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </View>
  );
};

export default Disclaimer;
