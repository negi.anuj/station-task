import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TextInput,
  FlatList,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import styles from '../../../styles';
import {StationList} from '../../services/helpers/stationModule';

const StationHome = props => {
  const [stationList, setStationList] = useState([]);
  const [searchData, setSearchData] = useState([]);
  const [searchStart, setSearchStart] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const selectStation = item => {
    props.navigation.navigate('StationDetails', {state: item});
  };

  const fetchStationList = () => {
    setIsLoading(true);
    StationList(res => {
      setStationList(res.data);
      setIsLoading(false);
    });
  };

  useEffect(() => {
    fetchStationList();
  }, []);

  const handleSearch = text => {
    let value = text.toLowerCase();
    let result = [];
    result = stationList.filter(data => {
      if (
        data?.name.toLowerCase().replace(/ +/g, '').search(value) !== -1 ||
        data?.pantone_value.toLowerCase().replace(/ +/g, '').search(value) !==
          -1 ||
        data?.id.toString().toLowerCase().replace(/ +/g, '').search(value) !==
          -1
      ) {
        return data;
      }
      if (
        (data?.name.toLowerCase().replace(/ +/g, '').search(value) !== -1) ===
          false ||
        (data?.pantone_value.toLowerCase().replace(/ +/g, '').search(value) !==
          -1) ===
          false ||
        (data?.id.toLowerCase().replace(/ +/g, '').search(value) !== -1) ===
          false
      ) {
        console.log('nothing');
      }
    });

    setSearchData(result);
    setSearchStart(true);
  };

  return (
    <View style={[styles.wrapper]}>
      {isLoading && <ActivityIndicator size="large" color="#DD1D21" />}
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Image
          style={{position: 'absolute', zIndex: -1, top: -140}}
          source={require('../../../assets/image/bottomBg.png')}
        />
      </TouchableWithoutFeedback>
      <View style={[styles.flexCenterColumn, {marginTop: 40}]}>
        <Text style={[styles.headerTextBold]}>Select Station</Text>
        <View style={[styles.contentWrapper, styles.flexCenterColumn]}>
          <View
            style={[
              styles.flexRow,
              styles.stationSearchInputContainer,
              {marginTop: 30, justifyContent: 'flex-start'},
            ]}>
            <Image source={require('../../../assets/image/search.png')} />
            <TextInput
              placeholder="Search by ID, Name, City"
              style={[styles.textInput, {marginLeft: 20}]}
              onChangeText={val => {
                handleSearch(val);
              }}
            />
          </View>

          <View style={[styles.listContainer]}>
            <FlatList
              keyExtractor={item => item.id}
              data={searchStart ? searchData : stationList}
              style={{width: '100%'}}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={
                <View style={{flex: 1, marginVertical: 100}}>
                  <Text style={{textAlign: 'center'}}>
                    Oops! No Stations Found
                  </Text>
                </View>
              }
              renderItem={({item, index}) => (
                <TouchableWithoutFeedback onPress={() => selectStation(item)}>
                  <View style={{width: '100%'}}>
                    <View style={[styles.stationCard]}>
                      <Image
                        source={require('../../../assets/image/pump.png')}
                      />
                      <View style={[styles.stationCardInfoText]}>
                        <Text style={[styles.infoHeadingText]}>
                          {item.pantone_value}
                        </Text>
                        <Text style={[styles.infoSubHeadingText]}>
                          {item.name}
                        </Text>
                      </View>
                    </View>
                    <View style={[styles.divider, {marginVertical: 10}]} />
                  </View>
                </TouchableWithoutFeedback>
              )}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default StationHome;
