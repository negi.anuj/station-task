import {
  View,
  Text,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  ScrollView
} from 'react-native';
import React, {useEffect, useState} from 'react';
import styles from '../../../styles';
import Button from '../../components/common/Button';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {initailValue, loginValidationSchema} from '../../utils/validation';
import {Formik} from 'formik';
import {LoginUser} from '../../services/helpers/userModule';
import * as Progress from 'react-native-progress';

const Login = props => {
  const [disclaimerTaken, setDisclaimerTaken] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const loginSubmit = payload => {
    setIsLoading(true);
    LoginUser(payload, setIsLoading, res => {
      setIsLoading(false);
      AsyncStorage.setItem('token', res.token).then(response => {
        disclaimerTaken
          ? props.navigation.reset({
              index: 0,
              routes: [{name: 'StationHome'}],
            })
          : props.navigation.reset({
              index: 0,
              routes: [{name: 'Disclaimer'}],
            });
      });
    });
  };

  useEffect(() => {
    AsyncStorage.getItem('disclaimer')
      .then(res => {
        setDisclaimerTaken(JSON.parse(res));
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    
    <ScrollView contentContainerStyle={[styles.wrapper]} showsVerticalScrollIndicator={false}>
     
        <View
          style={[
            styles.flexCenterColumn,
            {marginTop: 80, alignItems: 'center'},
          ]}>
          <Image
            style={[styles.imageCenter]}
            source={require('../../../assets/image/icon.png')}
          />
          <Text style={[styles.headerTextBold]}>Login</Text>
          <Formik
            initialValues={initailValue}
            validationSchema={loginValidationSchema}
            onSubmit={values => {
              loginSubmit(values);
            }}>
            {({handleChange, handleBlur, errors,touched, handleSubmit, values}) => (
              <>
                <View style={{marginTop: 20}}>
                  <View style={[styles.flexRow, styles.loginInputContainer]}>
                    <Image
                      source={require('../../../assets/image/mailIcon.png')}
                    />
                    <TextInput
                      placeholder="Email"
                      keyboardType="email-address"
                      style={[styles.textInput, {marginLeft: 20}]}
                      onChangeText={handleChange('email')}
                      name="email"
                      value={values.email}
                      onBlur={handleBlur('email')}
                    />
                  </View>
                  <View style={[styles.divider]} />

                  {errors.email && (values.email || touched.email) && (
                    <Text style={{fontSize: 10, color: 'red'}}>
                      {errors.email}
                    </Text>
                  )}
                  <View style={[styles.flexRow, styles.loginInputContainer]}>
                    <Image
                      source={require('../../../assets/image/passwordIcon.png')}
                    />
                    <TextInput
                      placeholder="Password"
                      secureTextEntry={true}
                      style={[styles.textInput, {marginLeft: 20}]}
                      onChangeText={handleChange('password')}
                      value={values.password}
                      onBlur={handleBlur('password')}
                    />
                  </View>
                  <View style={[styles.divider]} />
                  {errors.password && (values.password || touched.password) &&  (
                    <Text style={{fontSize: 10, color: 'red'}}>
                      {errors.password}
                    </Text>
                  )}
                </View>

                <TouchableWithoutFeedback
                  onPress={() => {
                    !isLoading && handleSubmit();
                  }}>
                  <View style={{margin: 20}}>
                    <Button
                      text={

                        isLoading ? <Progress.CircleSnail color={['white']} size={20}/> : 
                        <Text style={[styles.btnText]}>
                          Login{' '}
                          <Image
                            source={require('../../../assets/image/arrow.png')}
                          />
                        </Text>
                      }
                    />
                  </View>
                </TouchableWithoutFeedback>
              </>
            )}
          </Formik>
          <Text style={[styles.forgotPassword]}>Forgot Password?</Text>
        </View>
        <Image
          style={{position: 'absolute', zIndex: -1, bottom: 0}}
          source={require('../../../assets/image/bottomBg.png')}
        />
    </ScrollView>
  );
};

export default Login;
