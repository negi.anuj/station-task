import {
  getApiCall,
  postApiCall,
  putApiCall,
  deleteApiCall,
} from '../../config/axios';
import {BASE_URL, API} from '../../config/defines';

export const LoginUser = async (payload,setIsLoading, callback) => {
  const url = `${BASE_URL}${API.login}`;
  try {
    const response = await postApiCall(url, payload);
    if (response.error) {
      setIsLoading(false)
      alert(response.error)
    } else if (response.token) {
      callback(response);
    }
  } catch (error) {
    console.log(`Error in Copy cart ${error}`);
  }
};

export const ProductList = async (pageNum, callback) => {
  const url = `${BASE_URL}${API.ProductList}?pageNum=${pageNum}`;

  try {
    const response = await getApiCall(url);
    if (response.status === false) {
    } else if (response.status === true) {
      callback(response);
    }
  } catch (error) {
    console.log(`Error in Copy cart ${error}`);
  }
};
