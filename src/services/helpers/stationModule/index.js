import {
    getApiCall,
    postApiCall,
    putApiCall,
    deleteApiCall,
  } from '../../config/axios';
  import {BASE_URL, API} from '../../config/defines';
  
  export const StationList = async (callback) => {
    const url = `${BASE_URL}${API.stationList}`;
  
    try {
      const response = await getApiCall(url);
      if (!response.data) {
      } else if (response.data) {
        callback(response);
      }
    } catch (error) {
      console.log(`Error in Copy cart ${error}`);
    }
  };
  