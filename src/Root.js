import {View, Text} from 'react-native';
import React, {useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Root = props => {
  useEffect(() => {
    AsyncStorage.getItem('token')
      .then(res => {
        res
          ? props.navigation.reset({
              index: 0,
              routes: [{name: 'StationHome'}],
            })
          : props.navigation.reset({
              index: 0,
              routes: [{name: 'Login'}],
            });
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <View>
      <Text>..</Text>
    </View>
  );
};

export default Root;
