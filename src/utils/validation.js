import * as yup from 'yup'

export const initailValue = {
    email:"",
    password: ""
}

export const loginValidationSchema = yup.object().shape({
  email: yup
    .string()
    .email("Please enter valid email")
    .required('Email Address is Required'),
  password: yup
    .string()
    .min(5, "Password must be more than 5 characters")
    .required('Password is required'),
})