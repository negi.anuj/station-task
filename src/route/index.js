import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import StationHome from '../screens/StationHome';
import Login from '../screens/Login';
import Disclaimer from '../screens/Disclaimer';
import StationDetails from '../screens/StationDetails';
import Root from '../Root';

const Stack = createNativeStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Root">
        <Stack.Screen name="Root" component={Root}  />
        <Stack.Screen name="Login" component={Login}  />
        <Stack.Screen name="StationHome" component={StationHome} />
        <Stack.Screen name="Disclaimer" component={Disclaimer} />
        <Stack.Screen name="StationDetails" component={StationDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
