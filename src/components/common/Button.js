import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import styles from '../../../styles';

const Button = ({text}) => {
  return (
    <View style={[styles.btnMain]}>
      {text}
    </View>
  );
};

export default Button;

