import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import styles from '../../../styles';

const ButtonSlim = ({text}) => {
  return (
    <View style={[styles.btnMainSlim]}>
      {text}
    </View>
  );
};

export default ButtonSlim;

