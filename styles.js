const React = require('react-native');
const {Dimensions, Platform} = React;
const dheight = Dimensions.get('screen').height;
const dwidth = Dimensions.get('screen').width;
const fiveph = (dheight * 55) / 100;

const styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  flexCenterColumn: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  alignCenter: {
    alignItems: 'center',
  },
  headerTextBold: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 30,
    lineHeight: 31,
    textAlign: 'center',
  },
  forgotPassword: {
    fontSize: 12,
    color: '#000000',
    fontWeight: 'bold',
  },
  flexRow: {
    flexDirection: 'row',
  },
  loginInputContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 250,
  },
  stationSearchInputContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F0F4F5',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 12,
  },
  textInput: {
    width: '80%',
    marginLeft: 20,
  },
  divider: {
    backgroundColor: '#F0F4F5',
    height: 1,
    marginBottom: 7,
  },
  btnMain: {
    backgroundColor: '#DD1D21',
    borderRadius: 25,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
  btnMainSlim: {
    backgroundColor: '#DD1D21',
    borderRadius: 35,
    paddingHorizontal: 35,
    paddingVertical: 3,
  },
  btnText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '700',
  },
  contentWrapper: {
    backgroundColor: '#ffff',
    marginTop: 40,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingHorizontal: 30,
  },
  paraLight: {
    fontSize: 14,
    fontWeight: '400',
    color: '#000000',
    lineHeight: 27,
  },
  listContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    // backgroundColor:'red',
  },
  stationCard: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    marginVertical: 10,
  },
  stationCardInfoText: {
    marginLeft: 26,
  },
  infoHeadingText: {
    fontSize: 18,
    color: '#000000',
    fontWeight: 'bold',
    lineHeight: 27,
  },
  infoSubHeadingText: {
    fontSize: 14,
    color: '#ADB7C6',
    fontWeight: 'bold',
    lineHeight: 21,
  },
  detailCard: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 20,
    shadowColor: '#000000',
    shadowOffset: {width: 1, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 10,
    width: '100%',
    elevation: 5,
    marginTop: 20,
  },
  activeHeading: {
    color: '#000000',
    fontWeight: '700',
    fontSize: 16,
  },
  secondsText: {fontSize: 11, color: '#000000', fontWeight: '700'},
  secondTimer:{
    fontSize: 36, color: '#000000', fontWeight: '700'
  }
};

export default styles;
