import React from 'react';
import {SafeAreaView} from 'react-native';
import Route from './src/route';

const App = () => {
  return (
    <>
      <Route />
    </>
  );
};

export default App;
